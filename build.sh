#!/usr/bin/env bash

# exit when any command fails
set -e

if [ ! -d "./node_modules" ]; then
    npm install
fi

mkdir -p build/contracts
cp contracts/* build/contracts/

./node_modules/cashc/dist/main/cashc-cli.js --hex \
    ./contracts/two-option-vote.cash > ./build/contracts/two-option-vote.hex && \

npm run tsc --build tsconfig.json
