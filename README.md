# Two-option-vote

**[Project documentation](https://bitcoinunlimited.gitlab.io/two-option-vote/)**

A Bitcoin Cash smart contract and protocol for transparent on-chain voting.

"Two option vote" is a minimalistic and transparent voting protocol on top of
Bitcoin Cash and similar blockchains. Participating, verifying and tallying
does not require a full node, but is fully SPV client compatible. Votes are
non-transferable.

This repository hosts the protocol/smart contract specification and a reference
implementation in javascript.

- **[SPECIFICATION](https://bitcoinunlimited.gitlab.io/two-option-vote/two-option-vote-contract)**
- **[CODE REFERENCE](https://bitcoinunlimited.gitlab.io/two-option-vote/reference/)**

## Example

```ts
import {
  TwoOptionVote, TwoOptionVoteContract, tallyVotes, Blockchain,
  DEFAULT_CAST_TX_FEE, DUST, createSalt, derivePublicKey, getPublicKeyHash,
  generatePrivateKey,
} from 'two-option-vote';
const sleep = require('sleep');

// Initialize participants.
const alice = generatePrivateKey();
const bob = generatePrivateKey();

// Initialize an election
const votersPKH = [
  await getPublicKeyHash(await derivePublicKey(alice)),
  await getPublicKeyHash(await derivePublicKey(bob)),
];

const election: TwoOptionVote = {
  network: 'mainnet',
  salt: createSalt(),
  description: 'Pizza for lunch?',
  optionA: 'Yes',
  optionB: 'No',
  endHeight: 1_000_000,
  votersPKH,
};

// Setup contracts
const aliceContract = await TwoOptionVoteContract.make(election, alice);
const bobContract = await TwoOptionVoteContract.make(election, bob);

for (const contract of [aliceContract, bobContract]) {
  await contract.waitForBalance(DEFAULT_CAST_TX_FEE + DUST, () => {
    console.log(`Too low balance in contract ${contract.getContractAddress()}`);
  });
}
const electrum = new Blockchain();
await electrum.connect();

// Alice casts vote for option A ("Yes") and bob for B ("No")
const aliceTxID = await aliceContract.castVote(await aliceContract.optionAHash());
const bobTxID = await bobContract.castVote(await bobContract.optionBHash());

console.log(`Alice voted in tx ${aliceTxID}`);
console.log(`Bob voted in tx ${bobTxID}`);

// Tally votes on election
sleep.sleep(5); // Wait for transactions to propagate before tallying.

const includeUnconfirmed = true;
const tally = await tallyVotes(electrum, election, includeUnconfirmed);
console.log(`Results: ${JSON.stringify(tally, null, 4)}`);
```

See this and more examples in the [examples directory](https://gitlab.com/bitcoinunlimited/two-option-vote/-/tree/master/examples).

## Other implementations

A kotlin implementation is in the works by Bitcoin Unlimited.

### Test vectors

See `*.test.ts` files in [reference library](https://gitlab.com/bitcoinunlimited/two-option-vote/-/tree/master/).

## Build project
```bash
npm run build
npm run test
```

Run the above example: `node build/examples/readme-example.js`

## Contact

**[Join the Telegram channel](https://t.me/buip129)**
