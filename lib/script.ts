import InvalidVoteError from './error/invalidvoteerror';

export const OP_DATA_20 = 20;
export const OP_DATA_33 = 33;
export const OP_DATA_65 = 65;
export const OP_DATA_40 = 40;
export const OP_PUSHDATA1 = 0x4c;

const util = require('util');

/**
 * Throws an exception unless byte at position i is a specific opcode.
 *
 * @param script The full script
 * @param i Position within script
 * @param wantedOpcode The OP code we expect at position
 */
export function throwUnlessOpcode(script: Buffer, i: number, wantedOpcode: number) {
  if (script[i] === wantedOpcode) {
    return;
  }
  throw new InvalidVoteError(util.format(
    'Expected opcode %d at position %d, got %d',
    wantedOpcode, i, script[i],
  ));
}
