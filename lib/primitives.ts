import { CashCompiler, Contract, ElectrumNetworkProvider } from 'cashscript';
import * as path from 'path';
import { OP_DATA_20 } from './script';
import {
  hash160, signMessageWithSchnorr,
} from './crypto';
import { TwoOptionVote } from './twooptionvote';
import { hashToAddress } from './transaction';

const fs = require('fs');
const assert = require('assert');

/**
 * The proposal ID consist of a hash of all the data belonging to an election.
 *
 * @param election Election data.
 */
export async function calculateProposalID(election: TwoOptionVote): Promise<Uint8Array> {
  if (!(election.endHeight >= 0)) {
    throw Error('Invalid endheigt');
  }

  // use concat to take a copy
  const participants = election.votersPKH.concat().sort(Buffer.compare);

  const intToBuff = (n: number) => {
    const b = Buffer.alloc(4);
    b.writeUInt32BE(n, 0);
    return b;
  };

  const proposalIDParts: Buffer[] = [
    Buffer.from(election.salt),
    Buffer.from(election.description),
    Buffer.from(election.optionA),
    Buffer.from(election.optionB),
    intToBuff(election.endHeight),
    Buffer.concat(participants),
  ];

  const blob = Buffer.concat(proposalIDParts);
  return hash160(blob);
}

/**
 * Get the redeemscript of a two-option-vote contract.
 *
 * A transaction unlocking this script casts a vote.
 *
 * @param proposalID
 * @param optionA
 * @param optionB
 * @param voterPKH The voter who may solve this script.
 */
export function twoOptionContractRedeemscript(
  proposalID: Uint8Array, optionA: Uint8Array, optionB: Uint8Array, voterPKH: Uint8Array,
): Buffer {
  const hex = fs.readFileSync(path.join(__dirname,
    '../contracts/two-option-vote.hex'), 'utf8');
  const redeemScript = Buffer.from(hex, 'hex');

  return Buffer.concat([
    Buffer.alloc(1, OP_DATA_20), proposalID,
    Buffer.alloc(1, OP_DATA_20), optionB,
    Buffer.alloc(1, OP_DATA_20), optionA,
    Buffer.alloc(1, OP_DATA_20), voterPKH,
    redeemScript]);
}

/**
 * Find the two-option-vote contract bitcoin cash address for specified voter.
 *
 * @param proposalID
 * @param optionA
 * @param optionB
 * @param voterPKH The voter the address belongs to.
 */
export async function deriveContractAddress(
  proposalID: Uint8Array, optionA: Uint8Array, optionB: Uint8Array, voterPKH: Uint8Array,
): Promise<string> {
  const hash = await hash160(twoOptionContractRedeemscript(
    proposalID, optionA, optionB, voterPKH,
  ));

  return hashToAddress(hash, 'p2sh');
}

/**
 * Compiles the 'cashscript' contract file and creates a cashscript 'Contract'
 * instance from it.
 */
export function compileTwoOptionVote(
  network: 'mainnet' | 'testnet' | undefined,
  proposalID: Uint8Array,
  optA: Uint8Array,
  optB: Uint8Array,
  voterPKH: Uint8Array,
): Contract {
  const VoteContract = CashCompiler.compileFile(
    path.join(__dirname, '../contracts/two-option-vote.cash'),
  );

  const provider = new ElectrumNetworkProvider(network);
  return new Contract(VoteContract,
    [voterPKH, optA, optB, proposalID], provider);
}

/**
 * Vote message consists of proposal ID and a vote option concatinated.
 */
export function createVoteMessage(proposalID: Uint8Array, option: Uint8Array): Uint8Array {
  assert(proposalID.length === 20);
  assert(option.length === 20);
  return Buffer.concat([proposalID, option]);
}

/**
 * Signs a vote message with given private key. Vote message is proposal ID
 * and a vote option concatinated.
 */
export async function signVoteMessage(
  privateKey: Uint8Array, message: Uint8Array,
): Promise<Uint8Array> {
  assert(message.length === 40);
  return signMessageWithSchnorr(privateKey, message);
}
