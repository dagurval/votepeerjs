/**
 * The magical 20 bytes representing a blank vote.
 */
export const BLANK_VOTE: Uint8Array = Buffer.from('BEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', 'hex');

/**
 * Bitcoin cash output dust limit. Outputs cannot have less satoshis than this.
 */
export const DUST = 546;

/**
 * Approximate ~1 sat/byte fee for transaction casting a vote, assuming a
 * tarnsaction with 1 input + 1 output.
 */
export const DEFAULT_CAST_TX_FEE = 500;
