import { tallyVotes } from './tally';
import { getExampleElection } from './testutil';
import Blockchain from './blockchain';
import { TwoOptionVote } from './twooptionvote';

test('tallyVotes', async () => {
  jest.setTimeout(30000);
  const election = await getExampleElection();

  const chain = new Blockchain();
  await chain.connect();

  const result = await tallyVotes(chain, election, true);

  expect(result.optionA.length).toBe(1);
  expect(result.optionB.length).toBe(1);
  expect(result.blank.length).toBe(2);
  expect(result.noVote.length).toBe(1);
  expect(result.invalidVote.length).toBe(1);

  expect(result.optionA[0].voterPKH).toBe(election.votersPKH[0]);
  expect(result.optionB[0].voterPKH).toBe(election.votersPKH[1]);
  expect(result.blank[0].voterPKH).toBe(election.votersPKH[2]);
  expect(result.blank[1].voterPKH).toBe(election.votersPKH[3]);
  expect(result.invalidVote[0].voterPKH).toBe(election.votersPKH[4]);
  expect(result.noVote[0].voterPKH).toBe(election.votersPKH[5]);

  chain.disconnect();
});

/**
 * Test that duplicate votes in later blocks are ignored.
 *
 * In this example, one of the voters cast the same vote twice. However, one of
 * them confirmed before the other, so the vote does not get invalidated.
 */
test('ignore_later_votes', async () => {
  const votersPKH = [
    Buffer.from('4680a9baaa0fcd416742ccdbf6b302554acdb898', 'hex'),
    Buffer.from('d9ba804bbdebd5dcf41c0a49aacc135552e78e11', 'hex'),
  ];
  const salt = Buffer.from('4f56c48af8c73f48a7b9d91dce32303b4b6fe378', 'hex');

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt,
    description: 'Pizza for lunch?',
    optionA: 'Yes',
    optionB: 'No',
    endHeight: 1_000_000,
    votersPKH,
  };
  const electrum = new Blockchain();
  await electrum.connect();
  const tally = await tallyVotes(electrum, election, true);
  expect(tally.optionA.length).toBe(1);
  expect(tally.optionB.length).toBe(1);
  electrum.disconnect();
});
