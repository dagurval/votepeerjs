import {
  CashAddressType, CashAddressNetworkPrefix, decodeCashAddress, Transaction,
  encodeCashAddress, encodeTransaction,
} from '@bitauth/libauth';
import { sha256d } from './crypto';

const bufferReverse = require('buffer-reverse');

/**
 * Generate bitcoin cash address from PKH or SH.
 * @param hash Public key hash or script hash
 * @param addressType 'p2pkh' or 'p2sh'
 */
export function hashToAddress(hash: Uint8Array, addressType: string): string {
  if (addressType !== 'p2sh' && addressType !== 'p2pkh') {
    throw new Error('Unsupported address type');
  }

  let type = CashAddressType.P2PKH;
  if (addressType === 'p2sh') {
    type = CashAddressType.P2SH;
  }
  const prefix = CashAddressNetworkPrefix.mainnet;
  return encodeCashAddress(prefix, type, hash);
}

/**
 * Convert a bitcoin cash address to an output script.
 *
 * @param address cashaddr
 * @returns output script
 */
export function toOutputScript(address: string): Buffer {
  const decoded: any = decodeCashAddress(address);
  if (decoded.type === CashAddressType.P2PKH) {
    return Buffer.concat([
      Buffer.from([0x76]), // OP_DUP
      Buffer.from([0xa9]), // OP_HASH160
      Buffer.from([20]), // OP push 20 bytes
      decoded.hash,
      Buffer.from([0x88]), // OP_REQUALVERIFY
      Buffer.from([0xac]), // OP_CHECKSIG
    ]);
  }
  if (decoded.type === CashAddressType.P2SH) {
    return Buffer.concat([
      Buffer.from([0xa9]), // OP_HASH160
      Buffer.from([20]), // OP push 20 bytes
      decoded.hash,
      Buffer.from([0x87]), // OP_EQUAL
    ]);
  }
  throw Error('unknown cashaddr');
}

/**
 * Calculate the txid of a transaction
 * @param tx Bitcoin Cash transaction
 * @returns Transaction ID as hex string
 */
export async function getTxID(tx: Transaction): Promise<string> {
  const encoded = encodeTransaction(tx);
  const hashed = await sha256d(Buffer.from(encoded));
  return bufferReverse(hashed).toString('hex');
}

/**
 * If size could be a Bitcoin Cash ECDSA or Schnorr signature.
 */
export function isPlausibleSignatureSize(signatureSize: number) {
  return signatureSize >= 64 && signatureSize <= 73;
}
