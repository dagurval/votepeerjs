import {
  TwoOptionVote, TwoOptionVoteContract, tallyVotes, Blockchain,
  DEFAULT_CAST_TX_FEE, DUST, createSalt, derivePublicKey, getPublicKeyHash,
  generatePrivateKey,
} from '../lib';

const sleep = require('sleep');

/**
 * This is the voting example from the project README file.
 * It creates an election and performs two votes.
 */
async function run() {
  // Initialize participants.
  const alice = generatePrivateKey();
  const bob = generatePrivateKey();

  // Initialize an election
  const votersPKH = [
    await getPublicKeyHash(await derivePublicKey(alice)),
    await getPublicKeyHash(await derivePublicKey(bob)),
  ];

  const election: TwoOptionVote = {
    network: 'mainnet',
    salt: createSalt(),
    description: 'Pizza for lunch?',
    optionA: 'Yes',
    optionB: 'No',
    endHeight: 1_000_000,
    votersPKH,
  };

  // Setup contracts
  const aliceContract = await TwoOptionVoteContract.make(election, alice);
  const bobContract = await TwoOptionVoteContract.make(election, bob);

  for (const contract of [aliceContract, bobContract]) {
    await contract.waitForBalance(DEFAULT_CAST_TX_FEE + DUST, () => {
      console.log(`Too low balance in contract ${contract.getContractAddress()}`);
    });
  }
  const electrum = new Blockchain();
  await electrum.connect();

  // Alice casts vote for option A ("Yes") and bob for B ("No")
  const aliceTxID = await aliceContract.castVote(await aliceContract.optionAHash());
  const bobTxID = await bobContract.castVote(await bobContract.optionBHash());

  console.log(`Alice voted in tx ${aliceTxID}`);
  console.log(`Bob voted in tx ${bobTxID}`);

  // Tally votes on election
  sleep.sleep(5); // Wait for transactions to propagate before tallying.

  const includeUnconfirmed = true;
  const tally = await tallyVotes(electrum, election, includeUnconfirmed);
  console.log(`Results: ${JSON.stringify(tally, null, 4)}`);
}
run();
